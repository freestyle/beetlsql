BeetlSQL 所有API的例子

建议先参考 quickstart
如果需要扩展，可以参考plugin


*  S01MapperSelectSample 15个例子，  mapper中的查询演示
*  S02MapperUpdateSample 11个例子，  mapper中更新操作
*  S03MapperPageSample  3个例子，mapper中的翻页查询
*  S04QuerySample 9个例子，Query查询
*  S05QueryUpdateSample 3个例子，Query完成update操作
*  S06SelectSample 14个例子，SQLManager 查询API
*  S07InsertSample 8个例子，SQLManager 插入新数据API,主键生成
*  S08UpdateSample 6个例子,更新数据
*  S09JsonMappingSample 5个例子， json配置映射
*  S10FetchSample 2个例子，关系映射
*  S11BeetlFunctionSample 2个例子，自定义sql脚本的方法






